import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
})
export class ProductsComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
  products = [
    {
      id: 1,
      name: 'product1',
      price: '100',
      color: 'black',
      available: 'Available',
      image: './assets/products/img1.jfif',
    },
    {
      id: 2,
      name: 'product2',
      price: '200',
      color: 'white',
      available: 'Available',
      image: './assets/products/img2.jfif',
    },
    {
      id: 3,
      name: 'product3',
      price: '150',
      color: 'green',
      available: 'Not Available',
      image: './assets/products/img3.jfif',
    },
  ];
}
